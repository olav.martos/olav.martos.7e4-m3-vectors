﻿/*
 * AUTHOR: Olav Martos
 * DATE: 22/11/2022
 * DESCRIPTION: Conjunt de metodes enlaçats per un menu per treballar amb matrius. 
*/

using System;

namespace Matrius
{
    class Matrius
    {
        static void Main()
        {
            var menu = new Matrius();
            menu.Menu();
        }



        public void Menu()
        {
            var option = "";
            do
            {
                Console.Clear();
                Console.WriteLine("0. - Sortir del menu");
                Console.WriteLine("1. - SimpleBattleshipResult");
                Console.WriteLine("2. - MatrixElementSum");
                Console.WriteLine("3. - MatrixBoxesOpenedCounter");
                Console.WriteLine("4. - MatrixThereADiv13");
                Console.WriteLine("5. - HighestMountainOnMap");
                Console.WriteLine("6. - HighestMountainScalechange");
                Console.WriteLine("7. - MatrixSum");
                Console.WriteLine("8. - RookMoves");
                Console.WriteLine("9. - MatrixSimetric");
                Console.WriteLine("A. - MatrixProduct");
                Console.WriteLine("B. - SimpleMinesweeper");
                Console.WriteLine("C. - MagicSquare");
                Console.Write("Escull una opcio: ");

                option = Console.ReadLine();
                switch (option)
                {
                    case "1":
                        Console.Clear();
                        SimpleBattleshipResult();
                        break;
                    case "2":
                        Console.Clear();
                        MatrixElementSum();
                        break;
                    case "3":
                        Console.Clear();
                        MatrixBoxesOpenedCounter();
                        break;
                    case "4":
                        Console.Clear();
                        MatrixThereADiv13();
                        break;
                    case "5":
                        Console.Clear();
                        HighestMountainOnMap();
                        break;
                    case "6":
                        Console.Clear();
                        HighestMountainScalechange();
                        break;
                    case "7":
                        Console.Clear();
                        MatrixSum();
                        break;
                    case "8":
                        Console.Clear();
                        RookMoves();
                        break;
                    case "9":
                        Console.Clear();
                        MatrixSimetric();
                        break;
                    case "A":
                        Console.Clear();
                        MatrixProduct();
                        break;
                    case "B":
                        Console.Clear();
                        SimpleMinesweeper();
                        break;
                    case "C":
                        Console.Clear();
                        MagicSquare();
                        break;
                    case "0":
                        Console.Clear();
                        Console.WriteLine("Bye");
                        break;
                    default:
                        Console.WriteLine("Opció incorrecta!");
                        break;
                }
                Console.ReadLine();
            } while (option != "0");
        }



        /*Donada la següent configuració del joc Enfonsar la flota, indica si a la posició (x, y) hi ha aigua o un vaixell (tocat)*/
        public void SimpleBattleshipResult()
        {
            // Recuerdo de haberlo hecho por columnas en lugar de filas
            //char[,] field = { {'x', '0', '0', '0', '0', '0', 'x' }, { 'x', '0', '0', 'x', '0', '0', '0' }, { '0', 'x', '0', 'x', '0', '0', '0', }, { '0', '0', '0', 'x', '0', '0', '0' }, { '0', '0', '0', '0', 'x', 'x', '0', }, { '0', '0', '0', '0', '0', '0', '0' }, { 'x', 'x', 'x', 'x', '0', '0', '0' } };

            char[,] field = {
                { 'x', 'x', '0', '0', '0', '0', 'x' },
                { '0', '0', 'x', '0', '0', '0', 'x' },
                { '0', '0', '0', '0', '0', '0', 'x', },
                { '0', 'x', 'x', 'x', '0', '0', 'x' },
                { '0', '0', '0', '0', 'x', '0', '0', },
                { '0', '0', '0', '0', 'x', '0', '0' },
                { 'x', '0', '0', '0', '0', '0', '0' }
            };

            int coordX = 0;
            int coordY = 0;
            do
            {
                Console.Write("Introdueix la coordenada X (Recorda, del 0 al 6). Si vols parar introdueix -1: ");
                coordX = Convert.ToInt32(Console.ReadLine());

                Console.Write("Introdueix la coordenada Y (Recorda, del 0 al 6). Si vols parar introdueix -1: ");
                coordY = Convert.ToInt32(Console.ReadLine());

                if (coordX == -1 && coordY == -1)
                {
                    break;
                }

                if (field[coordX, coordY] == '0')
                {
                    Console.WriteLine("Aigua");
                }
                else
                {
                    Console.WriteLine("Tocat");
                }


            } while (coordX != -1 && coordY != -1);
            Console.WriteLine("Has sortit del programa");
        }



        /*Donada la següent matriu
         * matrix = {{2,5,1,6},{23,52,14,36},{23,75,81,64}}
         * Imprimeix la suma de tots els seus valors.
        */
        public void MatrixElementSum()
        {
            int[,] matrix = { { 2, 5, 1, 6 }, { 23, 52, 14, 36 }, { 23, 75, 81, 64 } };

            Console.Write("Vols fer la suma amb les columnes(1) o amb les files(2): ");
            string rowcol = Console.ReadLine();

            int suma = 0;

            switch (rowcol)
            {
                case "columnes":
                case "Columnes":
                case "1":
                    for (int i = 0; i < matrix.GetLength(1); i++)
                    {
                        for (int j = 0; j < matrix.GetLength(0); j++)
                        {
                            suma += matrix[j, i];
                        }
                    }
                    Console.WriteLine($"Suma columnes: {suma}");
                    break;
                case "files":
                case "Files":
                case "2":
                    for (int i = 0; i < matrix.GetLength(0); i++)
                    {
                        for (int j = 0; j < matrix.GetLength(1); j++)
                        {
                            suma += matrix[i, j];
                        }
                    }
                    Console.WriteLine($"Suma files: {suma}");
                    break;
            }
        }



        /*Un banc té tot de caixes de seguretat en una graella, numerades per fila i columna del 0 al 3.
         * Volem registrar quan els usuaris obren una caixa de seguretat, i al final del dia, fer-ne un recompte.
         * L'usuari introduirà parells d'enters del 0 al 3 quan s'obri la caixa indicada.
         * Quan introdueixi l'enter -1, és que s'ha acabat el dia. Printa per pantalla el nombre de cops que s'ha obert.
        */
        public void MatrixBoxesOpenedCounter()
        {
            int[,] boxes = { { 0, 0, 0, 0 }, { 0, 0, 0, 0 }, { 0, 0, 0, 0 }, { 0, 0, 0, 0 } };

            Console.WriteLine("Introdueix -1 quan finalitzi el dia, es a dir, quan ja s'hagin obert varies caixes");
            Console.Write("Numero de files del 0 al 3: ");
            int row = Convert.ToInt32(Console.ReadLine());
            while (row > 3 || row < 0)
            {
                Console.Write("Numero introduit incorrecte. Les files son del 0 al 3: ");
                row = Convert.ToInt32(Console.ReadLine());
            }

            while (row != -1)
            {
                switch (row)
                {
                    case 0:
                        Console.Write("Numero de columnes: ");
                        int col = Convert.ToInt32(Console.ReadLine());
                        while (col > 3 || col < 0)
                        {
                            Console.Write("Numero introduit incorrecte. Les columnes son del 0 al 3: ");
                            col = Convert.ToInt32(Console.ReadLine());
                        }
                        boxes[0, col]++;
                        break;
                    case 1:
                        Console.Write("Numero de columnes: ");
                        col = Convert.ToInt32(Console.ReadLine());
                        while (col > 3 || col < 0)
                        {
                            Console.Write("Numero introduit incorrecte. Les columnes son del 0 al 3: ");
                            col = Convert.ToInt32(Console.ReadLine());
                        }
                        boxes[1, col]++;
                        break;
                    case 2:
                        Console.Write("Numero de columnes: ");
                        col = Convert.ToInt32(Console.ReadLine());
                        while (col > 3 || col < 0)
                        {
                            Console.Write("Numero introduit incorrecte. Les columnes son del 0 al 3: ");
                            col = Convert.ToInt32(Console.ReadLine());
                        }
                        boxes[2, col]++;
                        break;
                    case 3:
                        Console.Write("Numero de columnes: ");
                        col = Convert.ToInt32(Console.ReadLine());
                        while (col > 3 || col < 0)
                        {
                            Console.Write("Numero introduit incorrecte. Les columnes son del 0 al 3: ");
                            col = Convert.ToInt32(Console.ReadLine());
                        }
                        boxes[3, col]++;
                        break;
                }
                Console.Write("Numero de files: ");
                row = Convert.ToInt32(Console.ReadLine());
                while (row > 3 || row < -1)
                {
                    Console.Write("El numero es superior o inferior als limits. Les filas son del 0 al 3 o -1 si el dia finalitza: ");
                    row = Convert.ToInt32(Console.ReadLine());
                }
            }


            for (int rowShow = 0; rowShow < boxes.GetLength(0); rowShow++)
            {
                for (int colShow = 0; colShow < boxes.GetLength(1); colShow++)
                {
                    Console.Write(" " + boxes[rowShow, colShow]);

                }
                Console.WriteLine();
            }
        }



        /*
         * Donada la següent matriu
         * matrix = {{2,5,1,6},{23,52,14,36},{23,75,81,62}}
         * Imprimeix true si algún dels números és divisible entre 13, false altrement.         
         */
        public void MatrixThereADiv13()
        {
            int[,] matrix = { { 2, 5, 1, 6 }, { 23, 52, 14, 36 }, { 23, 75, 81, 62 } };

            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    if (matrix[i, j] % 13 == 0)
                    {
                        Console.WriteLine("True"); // 52 es divisible entre 13. 13*4=52.
                    }
                }
            }
        }



        /*
         * Usant les imatges d'un satel·lit hem pogut fer raster o (mapa de bits)[https://ca.wikipedia.org/wiki/Mapa_de_bits] que ens indica l'alçada d'un punt concret d'un mapa. Hem obtingut la següent informació:
         * map ={{1.5,1.6,1.8,1.7,1.6},{1.5,2.6,2.8,2.7,1.6},{1.5,4.6,4.4,4.9,1.6},{2.5,1.6,3.8,7.7,3.6},{1.5,2.6,3.8,2.7,1.6}}
         *  
         * Digues en quin punt(x,y) es troba el cim més alt i la seva alçada
         */
        public void HighestMountainOnMap()
        {
            double[,] map = {
                { 1.5, 1.6, 1.8, 1.7, 1.6 },
                { 1.5, 2.6, 2.8, 2.7, 1.6 },
                { 1.5, 4.6, 4.4, 4.9, 1.6 },
                { 2.5, 1.6, 3.8, 7.7, 3.6 },
                { 1.5, 2.6, 3.8, 2.7, 1.6 }
            };

            int x = 0;
            int y = 0;
            double moreHigh = 0;
            for (int i = 0; i < map.GetLength(0); i++)
            {
                for (int j = 0; j < map.GetLength(1); j++)
                {
                    if (map[i, j] > moreHigh)
                    {
                        moreHigh = map[i, j];
                        x = i;
                        y = j;
                    }
                }
            }
            Console.WriteLine($"[{x}, {y}]: {moreHigh} metres");
        }



        /*
         * El govern britànic ens ha demanat que també vol accedir a les dades de l'exercici anterior i que les necessitaria en peus i no metres.
         * Per convertir un metre a peus has de multiplicar els metres per 3.2808.
         * Fes la conversió i imprimeix la matriu per pantalla.         
         */
        public void HighestMountainScalechange()
        {
            double[,] map = {
                { 1.5, 1.6, 1.8, 1.7, 1.6 },
                { 1.5, 2.6, 2.8, 2.7, 1.6 },
                { 1.5, 4.6, 4.4, 4.9, 1.6 },
                { 2.5, 1.6, 3.8, 7.7, 3.6 },
                { 1.5, 2.6, 3.8, 2.7, 1.6 }
            };

            int x = 0;
            int y = 0;
            double moreHigh = 0;

            for (int i = 0; i < map.GetLength(0); i++)
            {
                for (int j = 0; j < map.GetLength(1); j++)
                {
                    map[i, j] *= 3.2808;
                }
            }

            for (int row = 0; row < map.GetLength(0); row++)
            {
                for (int col = 0; col < map.GetLength(1); col++)
                {
                    Console.Write(" " + map[row, col]);

                }
                Console.WriteLine();
            }



            for (int i = 0; i < map.GetLength(0); i++)
            {
                for (int j = 0; j < map.GetLength(1); j++)
                {
                    if (map[i, j] > moreHigh)
                    {
                        moreHigh = map[i, j];
                        x = i;
                        y = j;
                    }
                }
            }
            Console.WriteLine($"\n\n\n[{x}, {y}]: {moreHigh} peus");
        }



        /*Implementa un programa que demani dos matrius a l'usuari i imprimeixi la suma de les dues matrius.*/
        public void MatrixSum()
        {
            // Creem la matriu
            Console.Write("Introdueix el numero de files de la primera matriu: ");
            int rowMatrix1 = Convert.ToInt32(Console.ReadLine());
            Console.Write("Introdueix el numero de columnes de la primera matriu: ");
            int colMatrix1 = Convert.ToInt32(Console.ReadLine());
            int[,] Matrix1 = new int[rowMatrix1, colMatrix1];

            // Guardem els valors en la matriu
            for (int i = 0; i < Matrix1.GetLength(0); i++)
            {
                for (int j = 0; j < Matrix1.GetLength(1); j++)
                {
                    Console.Write($"Introdueix un valor de la primera matriu per la posicio [{i},{j}]: ");
                    Matrix1[i, j] = Convert.ToInt32(Console.ReadLine());
                }
            }

            // Creem la segona matriu
            Console.Write("Introdueix el numero de files de la segona matriu: ");
            int rowMatrix2 = Convert.ToInt32(Console.ReadLine());
            // Per poder sumar dues matrius la condicio del while ha de ser false. Es a dir, que siguin iguals els valors
            while (rowMatrix2 != rowMatrix1)
            {
                Console.WriteLine("Per poder sumar dues matrius, han de tenir el mateix numero de files i columnes");
                Console.Write("Introdueix el numero de files de la segona matriu: ");
                rowMatrix2 = Convert.ToInt32(Console.ReadLine());
            }

            Console.Write("Introdueix el numero de columnes de la segona matriu: ");
            int colMatrix2 = Convert.ToInt32(Console.ReadLine());

            while (colMatrix2 != colMatrix1)
            {
                Console.WriteLine("Per poder sumar dues matrius, han de tenir el mateix numero de files i columnes");
                Console.Write("Introdueix el numero de columnes de la segona matriu: ");
                colMatrix2 = Convert.ToInt32(Console.ReadLine());
            }

            // Guardem els valors en la Matriu 2
            int[,] Matrix2 = new int[rowMatrix2, colMatrix2];

            for (int i = 0; i < Matrix2.GetLength(0); i++)
            {
                for (int j = 0; j < Matrix2.GetLength(1); j++)
                {
                    Console.Write($"Introdueix un valor de la segona matriu per la posició [{i},{j}]: ");
                    Matrix2[i, j] = Convert.ToInt32(Console.ReadLine());
                }
            }

            // Fem la suma dels valors.
            int[,] suma = new int[rowMatrix1, colMatrix2];

            for (int i = 0; i < suma.GetLength(0); i++)
            {
                for (int j = 0; j < suma.GetLength(1); j++)
                {
                    suma[i, j] = Matrix1[i, j] + Matrix2[i, j];
                }
            }

            // Mostrem la matriu resultant
            for (int row = 0; row < suma.GetLength(0); row++)
            {
                for (int col = 0; col < suma.GetLength(1); col++)
                {
                    Console.Write(" " + suma[row, col]);

                }
                Console.WriteLine();
            }
        }



        /*
         * Programa una funció que donat un tauler d'escacs, i una posició, ens mostri per pantalla quines són les possibles posicions a les que es pot moure una torre.
         * Primer caldrà llegir la posició de la torre, que simbolitzarem en el tauler amb el valor ♜ (fes copy & paste del símbol, és un string ja que ocupa dos chars), les posicions on aquesta es pugui moure les simbolitzarem amb el valor ♖ i la resta de posicions amb el valor x.
         * El moviment de la torre és el següent: Es pot moure al llarg d'una fila o d'una columna (però no successivament en una mateixa jugada); se la pot fer avançar tantes caselles com es vulgui.
         */
        public void RookMoves()
        {
            string brook = "&";
            string wrook = "$";

            Console.WriteLine("Degut a que no s'han pogut mostrar correctament els simbols de les dues torres hem optat per simbols que els reemplacin");
            Console.WriteLine("El simbol & es la torre que l'usuari introdueix\nEl simbol $ son les possibles possicions que pot tenir aquesta torre\n\n");
            string[,] chess = new string[8, 8];
            int vertical;
            int horitzontal = 0;

            // Omplim la matriu d'x.
            for (int i = 0; i < chess.GetLength(0); i++)
            {
                for (int j = 0; j < chess.GetLength(1); j++)
                {
                    chess[i, j] = "x";
                }
            }

            // El tauler d'escacs te les caselles horitzontals enumerades amb lletres de l'A a l'H
            Console.Write("Escriu la coordenada horitzontal on vols col·locar la torre (a-h): ");
            char horizon = Convert.ToChar(Console.ReadLine());

            // Impedim que puguin escriure majuscules o numeros
            while (horizon < 'a')
            {
                Console.Write("Rango (a-h): ");
                horizon = Convert.ToChar(Console.ReadLine());
            }

            // Impedim que no puguin escriu un caracter superior a l'h.
            while (horizon > 'h')
            {
                Console.WriteLine("No existeix aquesta posicio");
                Console.Write("Escriu la coordenada horitzontal on vols col·locar la torre: ");
                horizon = Convert.ToChar(Console.ReadLine());
            }
            // Convertim les lletres en numeros per la matriu
            switch (horizon)
            {
                case 'a':
                    horitzontal = 0;
                    break;
                case 'b':
                    horitzontal = 1;
                    break;
                case 'c':
                    horitzontal = 2;
                    break;
                case 'd':
                    horitzontal = 3;
                    break;
                case 'e':
                    horitzontal = 4;
                    break;
                case 'f':
                    horitzontal = 5;
                    break;
                case 'g':
                    horitzontal = 6;
                    break;
                case 'h':
                    horitzontal = 7;
                    break;
            }


            // El tauler d'escacs te les caselles verticals enumerades de l'1 al 8. Degut a que es fa amb Matrius, en aquest programa va del 0 al 8
            // On 0 es la casella 1 en tauler normal i 7 es la casella 8 en el tauler normal d'escacs
            Console.Write("Escriu la coordenada vertical on vols col·locar la torre: ");
            vertical = Convert.ToInt32(Console.ReadLine());
            while (vertical > 7 || vertical < 0)
            {
                Console.WriteLine("Estas sortint dels limits del tauler d'escacs. Els limits son del 0 al 7");
                Console.Write("Escriu la coordenada vertical on vols col·locar la torre: ");
                vertical = Convert.ToInt32(Console.ReadLine());
            }


            //Introduim la torre de l'usuari
            chess[vertical, horitzontal] = brook;


            // Bucles que calculen les possibles posicions de la torre en horitzontal
            for (int i = horitzontal - 1; i > -1; i--)
            {
                chess[vertical, i] = wrook;
            }

            for (int i = horitzontal + 1; i < 8; i++)
            {
                chess[vertical, i] = wrook;
            }


            // Bucles que calculen les possibles posicions de la torre en vertical
            for (int i = vertical - 1; i > -1; i--)
            {
                chess[i, horitzontal] = wrook;
            }

            for (int i = vertical + 1; i < 8; i++)
            {
                chess[i, horitzontal] = wrook;
            }

            // Mostra el tauler d'escacs
            for (int row = 0; row < chess.GetLength(0); row++)
            {
                for (int col = 0; col < chess.GetLength(1); col++)
                {
                    Console.Write(" " + chess[row, col]);

                }
                Console.WriteLine();
            }

        }



        /*Donada una matriu quadrada, el programa imprimeix true si la matriu és simètrica, false en cas contrari.*/
        public void MatrixSimetric()
        {
            // Creem la matriu 
            Console.Write("Introdueix el numero de filas: ");
            int row = Convert.ToInt32(Console.ReadLine());
            Console.Write("Introdueix el numero de columnes: ");
            int col = Convert.ToInt32(Console.ReadLine());
            int[,] matrix = new int[row, col];

            // Introduim els valors
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    Console.Write($"Introdueix un valor per la posicio [{i},{j}]: ");
                    matrix[i, j] = Convert.ToInt32(Console.ReadLine());
                }
            }

            // Mostrem la matriu
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    Console.Write(" " + matrix[i, j]);

                }
                Console.WriteLine();
            }

            // Diem si es simetrica o no.
            if (row == col) Console.WriteLine("Simetria: True.");
            else Console.WriteLine("Simetria: False.\t\tSolo son simetriques si el numero de files i columnes es igual");
        }



        /*Programa que multiplica dues matrius tenint en compte que han de tenir les mismes dimensions o si el numero de columnes de la Matriu 1 sigui igual al numero de files de la Matriu 2*/
        public void MatrixProduct()
        {
            // Matriu 1
            Console.Write("Numeros de filas de la matriu 1: ");
            int rowsA = Convert.ToInt32(Console.ReadLine());
            Console.Write("Numero de columnas de la matriu 1: ");
            int colsA = Convert.ToInt32(Console.ReadLine());
            double[,] matrixA = new double[rowsA, colsA];


            // Matriu 2
            Console.Write("Numero de filas de la matriu 2: ");
            int rowsB = Convert.ToInt32(Console.ReadLine());
            Console.Write("Numero de columnas de la matriu 2: ");
            int colsB = Convert.ToInt32(Console.ReadLine());
            double[,] matrixB = new double[rowsB, colsB];



            // Matriu de solució
            double[,] res = new double[rowsA, colsB];

            if (colsA != rowsB)
            {
                Console.WriteLine("La multiplicacio no es pot fer");
            }
            else
            {
                // Guardem les dades de la matriu 1
                for (int i = 0; i < rowsA; i++)
                {
                    for (int j = 0; j < colsA; j++)
                    {
                        Console.Write($"Valor para [{i}, {j}]: ");
                        matrixA[i, j] = Convert.ToDouble(Console.ReadLine());
                    }
                }

                Console.WriteLine("\n\n");

                // Guardem les dades de la matriu 2
                for (int i = 0; i < rowsB; i++)
                {
                    for (int j = 0; j < colsB; j++)
                    {
                        Console.Write($"Valor para [{i}, {j}]: ");
                        matrixB[i, j] = Convert.ToDouble(Console.ReadLine());
                    }
                }

                for (int a = 0; a < matrixB.GetLength(1); a++)
                {
                    for (int i = 0; i < matrixA.GetLength(0); i++)
                    {
                        for (int j = 0; j < matrixA.GetLength(1); j++)
                        {
                            res[i, a] += matrixA[i, j] * matrixB[j, a];
                        }
                    }
                }
            }


            // Mostrem el resultat
            for (int i = 0; i < res.GetLength(0); i++)
            {
                for (int j = 0; j < res.GetLength(1); j++)
                {
                    Console.Write(" " + res[i, j]);
                }
                Console.WriteLine();
            }

        }



        /*Programa que verifica si el Sudoku introduit es correcte o no*/
        public void SimpleMinesweeper()
        {
            Console.WriteLine("0 es el lloc on no es sap que et trobaras.\nOn hi hagi X hi havia una bomba\nOn hi ha un 1 es per on has caminat.");

            char[,] board = new char[10, 10];
            for (int i = 0; i < board.GetLength(0); i++)
            {
                for (int j = 0; j < board.GetLength(1); j++)
                {
                    board[i, j] = '0';
                }
            }

            for (int i = 0; i < board.GetLength(0); i++)
            {
                for (int j = 0; j < board.GetLength(1); j++)
                {
                    Console.Write(" " + board[i, j]);
                }
                Console.WriteLine();
            }

            for (int a = 0; a < board.GetLength(0); a++)
            {
                Random random = new Random();
                int rndI = random.Next(0, 10);
                int rndJ = random.Next(0, 10);
                if (board[rndI, rndJ] == '0')
                {
                    board[rndI, rndJ] = 'X';
                }
                else if (board[rndI, rndJ] == '1')
                {
                    board[rndI, rndJ] = '1';
                }
            }

            Console.WriteLine("Escriu -1 per terminar la partida, 1 per seguir jugant");
            int game = Convert.ToInt32(Console.ReadLine());
            while (game != -1)
            {
                Console.Write("Escriu el numero de fila que vols consultar: ");
                int userI = Convert.ToInt32(Console.ReadLine());
                while (userI < -1 && userI > 9)
                {
                    Console.WriteLine("No existeix");
                    Console.Write("Escriu el numero de fila que vols consultar: ");
                    userI = Convert.ToInt32(Console.ReadLine());
                }
                Console.Write("Escriu el numero de fila que vols consultar: ");
                int userJ = Convert.ToInt32(Console.ReadLine());
                while (userI < -1 && userI > 9)
                {
                    Console.WriteLine("No existeix");
                    Console.Write("Escriu el numero de fila que vols consultar: ");
                    userJ = Convert.ToInt32(Console.ReadLine());
                }

                if (board[userI, userJ] == '0')
                {
                    Console.WriteLine("No era una mina");
                    board[userI, userJ] = '1';
                    Console.WriteLine("Escriu -1 per terminar la partida, 1 per seguir jugant");
                    game = Convert.ToInt32(Console.ReadLine());
                }
                else if (board[userI, userJ] == 'X')
                {
                    Console.WriteLine("Era una mina, has mort");
                    game = -1;
                }
            }



            for (int i = 0; i < board.GetLength(0); i++)
            {
                for (int j = 0; j < board.GetLength(1); j++)
                {
                    Console.Write(" " + board[i, j]);
                }
                Console.WriteLine();
            }
        }



        /*
         * Donada una matriu quadrada la ompliu de valors enters on: 
         * La suma dels valors de qualsevol fila donen el mateix valor S
         * La suma dels valors de qualsevol columna dona el matiex valor S
         * La suma dels valors de qualsevol diagonal dona el mateix valor S         
         */
        public void MagicSquare()
        {
            // Preguntem la medida del quadrat magic
            Console.Write("Introdueix la medida del quadrat magic: ");
            int measure = Convert.ToInt32(Console.ReadLine());
            while (measure % 2 == 0)
            {
                Console.WriteLine("Aquest programa no pot resoldre quadrats magics pars, unicament pot impars");
                Console.Write("Introdueix la medida del quadrat magic: ");
                measure = Convert.ToInt32(Console.ReadLine());
            }


            int[,] square = new int[measure, measure];
            Console.WriteLine($"S'ha creat un quadrat {measure}x{measure}");

            // Creem dues variables per sapiguer de quina fila i columna parlem, juntament amb la diagonal
            int[] rows = new int[measure];
            int[] cols = new int[measure];
            int diags = 0;

            // Per a que es pugui resoldre de forma correcta s'ha de fer amb aquesta variable
            int num = 1;


            // Variables per poder calcular el quadrat magic
            int row = 0;
            int col = measure / 2;
            int rowAlter = row;
            int colAlter = col;

            // Poseem els primers valors dins del quadrat magic
            square[row, col] = num++;

            for (; num <= Math.Pow(measure, 2); num++)
            {
                row--;
                col++;

                if (row < 0) row = measure - 1;
                if (col >= measure) col = 0;

                if (square[row, col] == 0) square[row, col] = num;
                else
                {
                    rowAlter++;
                    if (rowAlter >= measure) rowAlter = 0;
                    square[rowAlter, colAlter] = num;
                    row = rowAlter;
                    col = colAlter;
                }
                rowAlter = row;
                colAlter = col;
            }


            for (row = 0; row < measure; row++)
            {
                for (col = 0; col < measure; col++)
                {
                    Console.Write(" {0}", square[row, col].ToString("###"));
                }
                Console.WriteLine("\n");
            }

            // Sumem els resultats en les llistes de files i columnes
            for (row = 0; row < measure; row++)
            {
                for (col = 0; col < measure; col++)
                {
                    rows[row] += square[row, col];
                    cols[col] += square[row, col];
                }
            }


            // Sumem els valors de la diagonal
            for (row = 0; row < square.GetLength(0) && row < square.GetLength(1); row++)
            {
                diags += square[row, row];
            }


            for (row = 0; row < measure; row++)
            {

                if (row == measure - 1)
                {
                    Console.WriteLine("Suma de la fila [{0}] es:\t\t{1}", row, rows[row]);
                    Console.WriteLine("Suma de la columna [{0}] es:\t{1}", row, cols[row]);
                }
                else
                {
                    Console.WriteLine("Suma de la fila [{0}] es:\t\t{1}", row, rows[row]);
                    Console.WriteLine("Suma de la columna [{0}] es:\t{1}", row, cols[row]);
                    Console.WriteLine("Suma de la digonal [{0}] es:\t{1}", row, diags);
                }
            }
        }
    }
}