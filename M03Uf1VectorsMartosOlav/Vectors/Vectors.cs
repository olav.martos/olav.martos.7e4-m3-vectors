﻿/*
 * AUTHOR: Olav Martos Aceña
 * DATE: 08/11/2022
 * DESCRIPTION: Conjunt de metodes enlaçats per un menu per treballar amb vectors.
 
 */

using System;

namespace Vectors
{
    class Vectors
    {
        static void Main()
        {
            var menu = new Vectors();
            menu.Menu();
        }



        public void Menu()
        {
            var option = "";
            do
            {
                Console.Clear();
                Console.WriteLine("0. - Sortir del menu");
                Console.WriteLine("1. - DayOfWeek");
                Console.WriteLine("2. - PlayerNumbers");
                Console.WriteLine("3. - CandidatesList");
                Console.WriteLine("4. - LetterInWord");
                Console.WriteLine("5. - AddValuesToList");
                Console.WriteLine("6. - Swap");
                Console.WriteLine("7. - PushButtonPadlockSimulator");
                Console.WriteLine("8. - BoxesOpenedCounter");
                Console.WriteLine("9. - MinOf10Values");
                Console.WriteLine("A. - IsThereAMultipleOf7");
                Console.WriteLine("B. - SearchInOrdered");
                Console.WriteLine("C. - InverseOrder");
                Console.WriteLine("D. - Palindrome");
                Console.WriteLine("E. - ListSortedValues");
                Console.WriteLine("F. - CapICuaValues");
                Console.WriteLine("10. - ListSameValues");
                Console.WriteLine("11. - ListSumValues");
                Console.WriteLine("12. - IvaPrices");
                Console.WriteLine("13. - CovidGrowRate");
                Console.WriteLine("14. - BicicleDistance");
                Console.WriteLine("15. - ValueNearAvg");
                Console.WriteLine("16. - Isbn");
                Console.WriteLine("17. - Isbn13");
                Console.WriteLine("Escull una opcio: ");

                option = Console.ReadLine();
                switch (option)
                {
                    case "1":
                        Console.Clear();
                        DayOfWeek();
                        break;
                    case "2":
                        Console.Clear();
                        PlayerNumbers();
                        break;
                    case "3":
                        Console.Clear();
                        CandidatesList();
                        break;
                    case "4":
                        Console.Clear();
                        LetterInWord();
                        break;
                    case "5":
                        Console.Clear();
                        AddValuesToList();
                        break;
                    case "6":
                        Console.Clear();
                        Swap();
                        break;
                    case "7":
                        Console.Clear();
                        PushButtonPadlockSimulator();
                        break;
                    case "8":
                        Console.Clear();
                        BoxesOpenedCounter();
                        break;
                    case "9":
                        Console.Clear();
                        MinOf10Values();
                        break;
                    case "a":
                    case "A":
                        Console.Clear();
                        IsThereAMultipleOf7();
                        break;
                    case "b":
                    case "B":
                        Console.Clear();
                        SearchInOrdered();
                        break;
                    case "c":
                    case "C":
                        Console.Clear();
                        InverseOrder();
                        break;
                    case "d":
                    case "D":
                        Console.Clear();
                        Palindrome();
                        break;
                    case "e":
                    case "E":
                        Console.Clear();
                        ListSortedValues();
                        break;
                    case "f":
                    case "F":
                        Console.Clear();
                        CapICuaValues();
                        break;
                    case "10":
                        Console.Clear();
                        ListSameValues();
                        break;
                    case "11":
                        Console.Clear();
                        ListSumValues();
                        break;
                    case "12":
                        Console.Clear();
                        IvaPrices();
                        break;
                    case "13":
                        Console.Clear();
                        CovidGrowRate();
                        break;
                    case "14":
                        Console.Clear();
                        BicicleDistance();
                        break;
                    case "15":
                        Console.Clear();
                        ValueNearAvg();
                        break;
                    case "16":
                        Console.Clear();
                        Isbn();
                        break;
                    case "17":
                        Console.Clear();
                        Isbn13();
                        break;
                    case "0":
                        Console.Clear();
                        Console.WriteLine("Bye");
                        break;
                    default:
                        Console.WriteLine("Opció incorrecta!");
                        break;
                }
                Console.ReadLine();
            } while (option != "0");
        }



        /*Donat un enter, printa el dia de la setmana amb text (dilluns, dimarts, dimecres…), tenint en compte que dilluns és el 0. Els dies de la setmana es guarden en un vector.*/
        public void DayOfWeek()
        {
            Console.WriteLine("Escriu un numero, per sapiguer quin es el dia de la setmana al que correspon: ");
            int nday = Convert.ToInt32(Console.ReadLine());
            while (nday > 6 || nday < 0)
            {
                Console.Clear();
                Console.WriteLine("No existeix cap dia de la setmana que correspongui amb aquest numero.");
                Console.WriteLine("Els dies de la setmana corresponen des del 0 al 6");
                Console.WriteLine("Escriu un numero, per sapiguer quin es el dia de la setmana al que correspon: ");
                nday = Convert.ToInt32(Console.ReadLine());
            }

            string[] day = { "dilluns", "dimarts", "dimecres", "dijous", "divendres", "dissabte", "diumenge" };
            Console.WriteLine(day[nday]);
        }



        /*Volem fer un petit programa per guardar l'alineació inicial de jugadors d'un partit de bàsquet. L'usuari introduirà els 5 números dels jugadors.*/
        public void PlayerNumbers()
        {
            Console.WriteLine("Introdueix el numero dels cinc jugadors");
            int[] playerN = new int[5];
            for (int i = 0; i < playerN.Length; i++)
            {
                playerN[i] = Convert.ToInt32(Console.ReadLine());
            }
            Console.Write("[");
            for (int j = 0; j < playerN.Length; j++)
            {
                if (j < (playerN.Length - 1))
                {
                    Console.Write($"{playerN[j]}, ");
                }
                else
                {
                    Console.Write($"{playerN[j]}");
                }

            }
            Console.Write("]");
            Console.WriteLine("");
        }



        /*  Volem fer un petit programa per un partit polític. Quan hi ha eleccions, el partit presenta una llista de candidats. Cada candidat té una posició a la llista.
            El programa primer llegirà la llista de candidats (primer introduirem la quantitat i després un candidat per línia).
            Un cop llegida la llista, l'usuari podrà preguntar quin candidat hi ha a cada posició. El programa acaba quan introdueixi -1. Tingues en compte que els polítics no són informàtics, i si indiquen la posició 1, volen dir el primer polític de la llista.
        */
        public void CandidatesList()
        {
            Console.WriteLine("Quants candidats hi ha?");
            int num = Convert.ToInt32(Console.ReadLine());
            string[] candidats = new string[num + 1];
            Console.WriteLine("Introdueix el nom dels candidats, cadascu en una linea:");
            for (int i = 0; i < num; i++)
            {
                candidats[i] = Console.ReadLine();
            }

            Console.Write("Introdueix el numero del candidat al que vols veure: ");
            int numAsk = Convert.ToInt32(Console.ReadLine()); ;
            while (numAsk != -1)
            {
                Console.WriteLine(candidats[numAsk - 1]);
                Console.Write("Introdueix el numero del candidat al que vols veure: ");
                numAsk = Convert.ToInt32(Console.ReadLine());
            }
            Console.WriteLine("Pulsa qualsevol tecla per finalitzar");
        }



        /*Donada una paraula i una posició  indica quina lletra hi ha a la posició indicada*/
        public void LetterInWord()
        {
            string[] word = new string[100];
            Console.Write("Introdueix una paraula: ");
            string userword = Console.ReadLine();
            for (int i = 0; i < (userword.Length); i++)
            {
                int num = userword.Length;
                //  Console.WriteLine(num);
                word[i] = Convert.ToString(userword[i]);

                // word[i] = Convert.ToString(userword[num]);
            }
            Console.Write("Introdueix un numero per sapiguer a quina lletra correspon: ");
            int numUser = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine(word[numUser]);
            Console.Write("");
        }



        /*  Inicialitza un vector de floats de mida 50, amb el valor 0.0f a tots els elements.
            Després assigna els valors següents a les posicions indicades i printa el vector:
            primera: 31.0f
            segona: 56.0f
            vintena: 12.0f
            última: 79.0f
        */
        public void AddValuesToList()
        {
            float[] element = new float[50];
            for (int i = 0; i < 50; i++)
            {
                element[i] = 0.0f;
            }
            element[0] = 31.0f;
            element[1] = 56.0f;
            element[19] = 12.0f;
            element[49] = 79.0f;
            Console.Write("[");
            for (int j = 0; j < 50; j++)
            {
                if (j < 49)
                {
                    Console.Write($"{element[j]}, ");
                }
                else
                {
                    Console.Write($"{element[j]}");
                }

            }
            Console.Write("]");
        }



        /*Donada un vector de 4 números de tipus int,intercanvia el primer per l'últim element.*/
        public void Swap()
        {
            int[] num = { 9, 4, 6, 7 };
            int temp = num[0];
            num[0] = num[3];
            num[3] = temp;
            Console.Write("[");
            for (int j = 0; j < 4; j++)
            {
                if (j < 3)
                {
                    Console.Write($"{num[j]}, ");
                }
                else
                {
                    Console.Write($"{num[j]}");
                }

            }
            Console.Write("]");
            Console.ReadLine();
        }



        /*
         * Volem fer un simulador d'un candau com el de la foto:
         * La nostra versió, també tindrà 8 botons, però el primer serà el 0. A l'inici tots els botons estaran sense prémer.
         * L'usuari introduirà enters indicant quin botó ha de prémer o no.
         * Quan introdueix el -1, és que ja ha acabat i hem d'imprimir l'estat del candau.
         */
        public void PushButtonPadlockSimulator()
        {
            bool[] status = { false, false, false, false, false, false, false, false };
            Console.WriteLine("Escriu un numero del 0 al 7");
            int num = Convert.ToInt32(Console.ReadLine());
            if (num == -1)
            {
                Console.WriteLine("El primer numero no pot ser -1. Ha de ser del 0 al 7");
            }
            while (num != -1)
            {
                switch (num)
                {
                    case 0:
                        status[0] = !(status[0]);
                        break;
                    case 1:
                        status[1] = !(status[1]);
                        break;
                    case 2:
                        status[2] = !(status[2]);
                        break;
                    case 3:
                        status[3] = !(status[3]);
                        break;
                    case 4:
                        status[4] = !(status[4]);
                        break;
                    case 5:
                        status[5] = !(status[5]);
                        break;
                    case 6:
                        status[6] = !(status[6]);
                        break;
                    case 7:
                        status[7] = !(status[7]);
                        break;
                }
                Console.WriteLine("Escriu un numero del 0 al 7");
                num = Convert.ToInt32(Console.ReadLine());
            }
            Console.Write("[");
            for (int i = 0; i < 8; i++)
            {
                if (i < 7)
                {
                    Console.Write($"{status[i]}, ");
                }
                else
                {
                    Console.Write($"{status[i]}");
                }

            }
            Console.Write("]");
        }



        /*Un banc té tot de caixes de seguretat, enumerades del 0 al 10.
         * Volem registrar quan els usuaris obren una caixa de seguretat, i al final del dia, fer-ne un recompte.
         * L'usuari introduirà enters del 0 al 10 quan s'obri la caixa indicada.
         * Quan introduiexi l'enter -1, és que s'ha acabat el dia. Printa per pantalla el nombre de cops que s'ha obert.
         */
        public void BoxesOpenedCounter()
        {
            int[] count = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            Console.WriteLine("Escriu un numero del 0 al 10");
            int num = Convert.ToInt32(Console.ReadLine());
            if (num == -1)
            {
                Console.WriteLine("El primer numero no pot ser -1. Ha de ser del 0 al 10");
            }
            while (num != -1)
            {
                switch (num)
                {
                    case 0:
                        count[0]++;
                        break;
                    case 1:
                        count[1]++;
                        break;
                    case 2:
                        count[2]++;
                        break;
                    case 3:
                        count[3]++;
                        break;
                    case 4:
                        count[4]++;
                        break;
                    case 5:
                        count[5]++;
                        break;
                    case 6:
                        count[6]++;
                        break;
                    case 7:
                        count[7]++;
                        break;
                    case 8:
                        count[8]++;
                        break;
                    case 9:
                        count[9]++;
                        break;
                    case 10:
                        count[10]++;
                        break;
                }
                Console.WriteLine("Escriu un numero del 0 al 10");
                num = Convert.ToInt32(Console.ReadLine());
            }
            Console.Write("[");
            for (int i = 0; i < 11; i++)
            {
                if (i < 10)
                {
                    Console.Write($"{count[i]}, ");
                }
                else
                {
                    Console.Write($"{count[i]}");
                }

            }
            Console.Write("]");
        }



        /*L'usuari entra 10 enters. Crea un vector amb aquest valors. Imprimeix per pantalla el valor més petit introduït.*/
        public void MinOf10Values()
        {
            Console.WriteLine("Introdueix 10 numeros");
            int[] numList = new int[10];


            for (int i = 0; i < numList.Length; i++)
            {
                numList[i] = Convert.ToInt32(Console.ReadLine());
            }
            int minim = numList[0];
            for (int j = 0; j < numList.Length; j++)
            {
                if (numList[j] < minim)
                {
                    minim = numList[j];
                }
            }
            Console.WriteLine($"El numero més petit es el {minim}");
        }



        /*Donat el següent vector, imprimeix true si algun dels números és divisible entre 7 o false en cas contrari.*/
        public void IsThereAMultipleOf7()
        {
            int[] values = { 4, 8, 9, 40, 54, 84, 40, 6, 84, 1, 1, 68, 84, 68, 4, 840, 684, 25, 40, 98, 54, 687, 31, 4894, 468, 46, 84687, 894, 40, 846, 1681, 618, 161, 846, 84687, 6, 848 };
            for (int i = 0; i < values.Length; i++)
            {
                if (values[i] % 7 == 0)
                {
                    Console.WriteLine("True");
                }
                else
                {
                    Console.WriteLine("False");
                }
            }
        }



        /*Donat una llista d'enters ordenats de menor a major indica si un cert valor existeix en el bucle.*/
        public void SearchInOrdered()
        {
            Console.WriteLine("Introdueix la quantitat de numeros que vols");
            int numList = Convert.ToInt32(Console.ReadLine());
            int[] list = new int[numList];


            for (int i = 0; i < numList; i++)
            {
                Console.WriteLine("Introdueix els numeros de menor a major");
                int num = Convert.ToInt32(Console.ReadLine());
                if (i == 0)
                {
                    list[i] = num;
                }
                else if (num < list[i - 1])
                {
                    Console.WriteLine("El numero ha de ser major a l'anterior");
                }
                else
                {
                    list[i] = num;
                }
            }
            Console.Write("Escriu el numero que vols sapiguer si es troba o no: ");
            int numAsk = Convert.ToInt32(Console.ReadLine());
            for (int i = 0; i < list.Length; i++)
            {
                if (list[i] == numAsk)
                {
                    Console.WriteLine("True");
                }
                else
                {
                    Console.WriteLine("False");
                    break;
                }
            }
        }



        /*L'usuari entra 10 enters. Imprimeix-los en l'ordre invers al que els ha entrat.*/
        public void InverseOrder()
        {
            Console.WriteLine("Introdueix deu numeros");
            int[] nums = new int[10];
            int number = 9;
            for (int i = 0; i < 10; i++)
            {
                if (i == 0)
                {
                    nums[number] = Convert.ToInt32(Console.ReadLine());
                }
                else
                {
                    number--;
                    nums[number] = Convert.ToInt32(Console.ReadLine());
                }
            }
            Console.Write("[");
            for (int j = 0; j < 10; j++)
            {
                if (j < 9)
                {
                    Console.Write($"{nums[j]}, ");
                }
                else
                {
                    Console.Write($"{nums[j]}");
                }

            }
            Console.Write("]");
        }



        /*  Indica si una paraula és palíndrom.
         *  Exemples de palíndroms:
         *  cec, cuc, gag, mínim, nan, nen, pipiripip…
        */
        public void Palindrome()
        {
            string palindrome = "";
            Console.Write("Escriu la paraula per saber si es palindrom: ");
            string word = Console.ReadLine().ToLower();
            for (int i = word.Length - 1; i >= 0; i--)
            {
                palindrome += word[i];
            }
            if (palindrome == word)
            {
                Console.WriteLine($"{word} es una paraula palindrom.");
            }
            else
            {
                Console.WriteLine($"{word} No es una paraula palindrom.");
            }
        }



        /* Printa per pantalla ordenats si la llista de N valors introduïts per l'usuari estan ordenats.
         * L'usuari primer entrarà el número d'enters a introduir i després els diferents enters
         */
        public void ListSortedValues()
        {

            int status = 0;
            int cont = 1;

            Console.Write("Insereix la quantitat total de numeros que vas a introduir: ");
            int numUser = Convert.ToInt32(Console.ReadLine());
            int[] llistavalors = new int[numUser];

            for (int i = 0; i < numUser; i++)
            {
                Console.Write("Escriu un valor per la llista: ");
                llistavalors[i] = Convert.ToInt32(Console.ReadLine());
            }
            while (cont < llistavalors.Length)
            {
                if (llistavalors[cont] < llistavalors[cont - 1])
                {
                    status = 1;
                }
                cont++;
            }
            if (status == 0)
            {
                Console.WriteLine("Ordenats");
            }
            else
            {
                Console.WriteLine("Desordenats");
            }
        }


        /*Printa per pantalla cap i cua si la llista de N valors introduïts per l'usuari són cap i cua (llegits en ordre invers és la mateixa llista).*/
        public void CapICuaValues()
        {
            Console.Write("Introdueix la quantitat de valors que vols introduir: ");
            int numUser = Convert.ToInt32(Console.ReadLine());
            int[] values = new int[numUser];
            for (int i = 0; i < numUser; i++)
            {
                Console.WriteLine("Introdueix els numeros per sapiguer si son cap i cua");
                values[i] = Convert.ToInt32(Console.ReadLine());
            }

            int status = 1;
            for (int j = 0; j < numUser; j++)
            {
                if (values[j] == values[numUser - 1])
                {
                    status = 0;
                    numUser--;
                }
                else
                {
                    status = 1;
                    numUser--;
                }
            }
            if (status == 0)
            {
                Console.WriteLine("Es una llista cap i cua");
            }
            else
            {
                Console.WriteLine("No es cap i cua");
            }
        }



        /*L'usuari introduirà 2 vectors de valors, primer mida després introdueix elements. 
         * Printa per pantalla són iguals si ha introduït el mateix vector, o no són iguals si són diferents.*/
        public void ListSameValues()
        {
            Console.Write("Introdueix la mida del primer vector: ");
            int mida1 = Convert.ToInt32(Console.ReadLine());
            int[] vec1 = new int[mida1];
            for (int i = 0; i < mida1; i++)
            {
                Console.Write("Introdueix un valor: ");
                vec1[i] = Convert.ToInt32(Console.ReadLine());
            }

            Console.Write("Introdueix la mida del segon vector: ");
            int mida2 = Convert.ToInt32(Console.ReadLine());

            while (mida2 != mida1)
            {
                Console.WriteLine("No es poden comparar si tenen diferents longituds");
                Console.Write("Introdueix la mida del segon vector: ");
                mida2 = Convert.ToInt32(Console.ReadLine());
            }

            int[] vec2 = new int[mida2];
            for (int j = 0; j < mida2; j++)
            {
                Console.Write("Introdueix un valor: ");
                vec2[j] = Convert.ToInt32(Console.ReadLine());
            }


            int count = 0;
            for (int k = 0; k < mida2; k++)
            {
                if (vec1[k] == vec2[k])
                {
                    count++;
                }
            }

            if (count == mida2 && count == mida1)
            {
                Console.WriteLine("Son iguals");
            }
            else
            {
                Console.WriteLine("No son iguals");
            }
        }



        /*L'usuari introdueix una llista de valors.
         * Imprimeix per pantalla la suma d'aquests valors.*/
        public void ListSumValues()
        {
            Console.Write("Quan mideix la teva llista: ");
            int mida = Convert.ToInt32(Console.ReadLine());
            int[] values = new int[mida];
            for (int i = 0; i < mida; i++)
            {
                Console.Write("Introdueix un valor de la llista: ");
                values[i] = Convert.ToInt32(Console.ReadLine());
            }
            int sum = 0;
            for (int j = 0; j < mida; j++)
            {
                sum += values[j];
            }
            Console.WriteLine($"La suma dels {mida} valors es: {sum}");
        }



        /*En una botiga volem convertir tot de preus sense a IVA al preu amb IVA. Per afegir l'IVA a un preu hem de sumar-hi el 21% del seu valor.
         * L'usuari introduirà el preu de 10 articles. Imprimeix per pantalla el preu amb l'IVA afegit amb el següent format indicat a continuació. 
         * El programa no pot imprimir res fins a que hagi llegit tots els valors.
        */
        public void IvaPrices()
        {
            Console.WriteLine("Has d'introduir 10 preus sense IVA i es calculara el seu valor amb IVA quan tu acabis");
            double[] preus = new double[10];
            double[] preusIVA = new double[10];

            for (int i = 0; i < preus.Length; i++)
            {
                Console.Write("Introdueix un preu sense IVA: ");
                preus[i] = Convert.ToInt32(Console.ReadLine());
            }

            Console.Clear();
            for (int j = 0; j < preus.Length; j++)
            {
                preusIVA[j] = preus[j] + preus[j] * 21 / 100;
                Console.WriteLine($"{preus[j]}.0 IVA = {preusIVA[j]}");
            }
        }



        /*El departament de salut ens ha demanat que calculem la taxa d’infecció que està tenint la Covid en la nostre regió sanitària. Donat un nombre de casos 
         * casos1
         * casos1 en una setmana, si la següent tenim un nombre de casos
         * casos2
         * casos2, podem calcular la taxa d'infecció amb la fórmula
         * infecció =  casos2/casos1
         * L'usuari introduirà un llistat de casos detectats cada setmana. Imprimeix la taxa d'infecció detectada cada setmana. El programa no pot imprimir res fins a que hagi llegit tots els valors.
*/
        public void CovidGrowRate()
        {
            Console.Write("Introdueix el numero de setmanes que vols avaluar: ");
            double[] rate = new double[Convert.ToInt32(Console.ReadLine())];
            for (int i = 0; i < rate.Length; i++)
            {
                Console.Write("Escriu els casos ocurreguts: ");
                rate[i] = Convert.ToInt32(Console.ReadLine());
            }

            for (int j = 1; j < rate.Length; j++)
            {
                Console.Write($"{rate[j] / rate[j - 1]}\t");
            }
        }



        /*Donada una velocitat d'una bicicleta en metres per segon, indica els metres que haurà recorregut quan hagi passat 1,2,3,4,5,6,7,8,9 i 10 segons.*/
        public void BicicleDistance()
        {
            double[] t = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
            Console.Write("Introdueix la velocitat de la bicicleta: ");
            double v = Convert.ToDouble(Console.ReadLine());
            for (int i = 0; i < t.Length; i++)
            {
                if (t[i] == 1)
                {
                    Console.WriteLine($"En {t[i]} segon recorre {v * t[i]} metres");
                }
                else
                {
                    Console.WriteLine($"En {t[i]} segons recorre {v * t[i]} metres");
                }
            }
        }



        /*L'usuari introdueix una llista de valors. 
         * Imprimeix per pantalla el valor que està més proper a la mitjana dels valors de la llista (calcula la mitjana dels valors primer 
         * i cerca el més proper després).*/
        public void ValueNearAvg()
        {
            Console.Write("Introdueix la longitud de la teva llista: ");
            int[] values = new int[Convert.ToInt32(Console.ReadLine())];
            double media = 0;
            for (int i = 0; i < values.Length; i++)
            {
                Console.Write("Introdueix un valors: ");
                values[i] = Convert.ToInt32(Console.ReadLine());
                media += values[i];
            }
            media /= values.Length;

            double max = 0;
            for (int j = values.Length - 1; j >= 0; j--)
            {
                if (values[j] > media)
                {
                    max = values[j];
                }
                else if (values[j] == media)
                {
                    max = media;
                }
                else
                {
                    max = values[j - 1];
                }
            }

            Console.WriteLine(max);

        }



        /*Verificar si un ISBN es valid o no*/
        public void Isbn()
        {
            int[] isbn = new int[11];
            isbn[0] = 0;
            int sum = 0;
            for (int i = 1; i < isbn.Length; i++)
            {
                Console.Write("Escriu un digit del teu isbn: ");
                isbn[i] = Convert.ToInt32(Console.ReadLine());
                sum += isbn[i] * i;
            }
            if (sum % 11 == isbn[isbn.Length - 1])
            {
                Console.WriteLine("True");
            }
            else
            {
                Console.WriteLine("False");
            }

        }



        /*Verificar si un ISBN13 es valid o no*/
        public void Isbn13()
        {
            int[] isbn = new int[14];
            isbn[0] = 0;
            int sum = 0;
            for (int i = 1; i < isbn.Length - 1; i++)
            {
                Console.Write("Introdueix un digit del teu isbn: ");
                isbn[i] = Convert.ToInt32(Console.ReadLine());
                if (i % 2 == 0)
                {
                    sum += isbn[i] * 3;
                }
                else
                {
                    sum += isbn[i] * 1;
                }
            }
            Console.Write("Introdueix un digit del teu isbn: ");
            sum += Convert.ToInt32(Console.ReadLine());
            if (sum % 10 == 0)
            {
                Console.WriteLine("True");
            }
            else
            {
                Console.WriteLine("False");
            }
        }
    }
}
